# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/input/touchscreen/focaltech,fts.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Focaltech FTS I2C Touchscreen Controller

maintainers:
  - Caleb Connolly <caleb@connolly.tech>

allOf:
  - $ref: touchscreen.yaml#

properties:
  compatible:
    enum:
      - focaltech,fts5452
      - focaltech,fts8719
  reg:
    const: 0x38

  interrupts:
    maxItems: 1

  reset-gpios:
    maxItems: 1

  wakeup-source:
    type: boolean
    description: touchscreen can be used as a wakeup source.

  focaltech,max-touch-number:
    $ref: /schemas/types.yaml#/definitions/uint32
    description: max number of fingers supported
    minimum: 2
    maximum: 10

  touchscreen-size-x: true
  touchscreen-size-y: true

additionalProperties: false

required:
  - compatible
  - reg
  - reset-gpios
  - focaltech,max-touch-number
  - touchscreen-size-x
  - touchscreen-size-y

examples:
  - |
    #include <dt-bindings/interrupt-controller/irq.h>
    #include <dt-bindings/gpio/gpio.h>
    &i2c5 {
      status="okay";

      touchscreen: focaltech@38 {
        compatible = "focaltech,fts8719";
        reg = <0x38>;
        wakeup-source;
        interrupt-parent = <&tlmm>;
        interrupts = <125 0x2>;
        vdd-supply = <&vreg_l28a_3p0>;
        vcc-i2c-supply = <&vreg_l14a_1p88>;

        pinctrl-names = "default", "suspend";
        pinctrl-0 = <&ts_int_active &ts_reset_active>;
        pinctrl-1 = <&ts_int_suspend &ts_reset_suspend>;

        reset-gpio = <&tlmm 99 GPIO_ACTIVE_HIGH>;
        irq-gpio = <&tlmm 125 GPIO_TRANSITORY>;
        touchscreen-size-x = <1080>;
        touchscreen-size-y = <2160>;
        focaltech,max-touch-number = <5>;
      };
    };
